<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'password'],function() {
	Route::post('/email', 'Auth\ForgotPasswordController@getResetToken');
	Route::post('/reset', 'Auth\ResetPasswordController@reset');
});

Route::group(['prefix'=> 'auth'],function(){
    Route::post('/register','Auth\RegisterController@register');
    Route::post("/login",'Auth\LoginController@login');
    Route::post('/login/{social}/callback','Auth\LoginController@handleProviderCallback')->where('social','twitter|facebook|linkedin|google|');
});
Route::resource('blog','Front\BlogController',[ 'except' => ['edit','create'] ]);
Route::resource('posts','Front\PostController',[ 'except' => ['edit','create'] ]);
Route::any('/get/posts/{id}','Front\PostController@getUserPosts');
Route::any('/get/news/{id}','Front\NewsController@getUserNews');
Route::any('/get/blog/{id}','Front\BlogController@getUserBlogs');
Route::get('menu','Front\MenuController@index');
Route::get('getLocale','Front\MenuController@getLocale');
Route::get('setLocale/{lang}','Front\MenuController@setLocale');
Route::resource('news','Front\NewsController',[ 'except' => ['edit','create'] ]);

Route::middleware(['jwt_auth'])->group(function(){
   Route::get('/hello',function(){
       return "Cool dude";
   });
});
