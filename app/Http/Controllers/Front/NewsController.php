<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\News;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (isset($_GET['page'])):
            $news = News::orderBy('order')->paginate(3);
        else:
            $news = News::orderBy('order')->get();
        endif;
        return response()->json($news);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $new = News::find($id);
        return response()->json($new);
    }
    public function store(Request $request){
        if ($request->hasFile('image')) {
            if ($request->file('image')->isValid()) {
                try {
                    $file = $request->file('image');
                    $path = 'news';
                    $iname = rand(11111, 99999) . '.' . $file->getClientOriginalExtension();
                    $name = $path . '/' . $iname;
                    $request->file('image')->move("uploads/news", $iname);

                } catch (Illuminate\Filesystem\FileNotFoundException $e) {

                }
            }
        }else {
            $name = '';
        }
        $store = News::store($request,$name);
        return $store;
    }
    public function update(Request $request, $id)
    {
        $news = News::find($id);
        $news->title = $request['title'];
        $news->description = $request['description'];
        $news->save();
        return $news;
    }
    public function getUserNews($id){
        $news = News::where('author_id',$id)->paginate(25);
        return response()->json($news);
    }

    public function destroy($id){
        return News::destroy($id);
    }

}
