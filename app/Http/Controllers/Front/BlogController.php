<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if(isset($_GET['page'])) {
            $blog = Blog::orderBy('order')->paginate(3);
        }else{
            $blog = Blog::orderBy('order')->get();
        }

        return response()->json($blog);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $blog = Blog::find($id);
        return response()->json($blog);
    }

    public function store(Request $request){
        if ($request->hasFile('image')) {
            if ($request->file('image')->isValid()) {
                try {
                    $file = $request->file('image');
                    $path = 'blog';
                    $iname = rand(11111, 99999) . '.' . $file->getClientOriginalExtension();
                    $name = $path . '/' . $iname;
                    $request->file('image')->move("uploads/blog", $iname);

                } catch (Illuminate\Filesystem\FileNotFoundException $e) {

                }
            }
        }else {
            $name = '';
        }
        $store = Blog::store($request,$name);
        return $store;
    }
    public function update(Request $request, $id)
    {
        $blog = Blog::find($id);
        $blog->title = $request['title'];
        $blog->body = $request['body'];
        $blog->save();
        return $blog;
    }
    public function getUserBlogs($id){
        $blog = Blog::where('author_id',$id)->paginate(25);
        return response()->json($blog);
    }

    public function destroy($id){
        return Blog::destroy($id);
    }

}
