<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use TCG\Voyager\Models\MenuItem;
use Illuminate\Support\Facades\App;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return(MenuItem::where('menu_id','=',2)->orderBy('order')->get());
    }

}
