<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Post;
use Illuminate\Http\Request;
class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(isset($_GET['page'])) {
            $posts = Post::where('status', '=', 'PUBLISHED')->orderBy('order')->paginate(25);
        }else{
            $posts = Post::where('status','=','PUBLISHED')->orderBy('order')->get();
        }
        return response()->json($posts);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public  function show($id)
    {
        $post = Post::find($id);
        return response()->json($post);
    }

    public function store(Request $request){
        if ($request->hasFile('image')) {
            if ($request->file('image')->isValid()) {
                try {
                    $file = $request->file('image');
                    $path = 'posts';
                    $iname = rand(11111, 99999) . '.' . $file->getClientOriginalExtension();
                    $name = $path . '/' . $iname;
                    $request->file('image')->move("uploads/posts", $iname);

                } catch (Illuminate\Filesystem\FileNotFoundException $e) {

                }
            }
        }else {
            $name = '';
        }
        $store = Post::store($request,$name);
        return $store;
    }
    public function update(Request $request, $id)
    {
        $post = Post::find($id);
        $post->title = $request['title'];
        $post->body = $request['body'];
        $post->excerpt = $request['excerpt'];
        $post->save();
        return $post;
    }
    public function getUserPosts($id){
        $posts = Post::where('author_id',$id)->paginate(25);
        return response()->json($posts);
    }

    public function destroy($id){
        return Post::destroy($id);
    }

}
