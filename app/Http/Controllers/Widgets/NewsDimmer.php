<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 12/25/2018
 * Time: 4:17 PM
 */

namespace App\Http\Controllers\Widgets;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Widgets\BaseDimmer;
use App\Models\News;

class NewsDimmer extends BaseDimmer
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $count = News::all()->count();
        $string = trans_choice('News', $count);
        $text = 'You have '.$count.' news in your database. Click on button below to view all news.';
        return view('voyager::dimmer', array_merge($this->config, [
            'icon'   => 'voyager-receipt',
            'title'  => "{$count} {$string}",
            'text'   => __($text),
            'button' => [
                'text' => __('View all news'),
                'link' => url('/admin/news'),
            ],
            'image' => asset('images/Widgets/images.jpg'),
        ]));
    }

    /**
     * Determine if the widget should be displayed.
     *
     * @return bool
     */
    public function shouldBeDisplayed()
    {
        return Auth::user()->can('browse', Voyager::model('User'));
    }
}