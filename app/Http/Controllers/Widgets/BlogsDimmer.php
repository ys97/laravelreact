<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 12/25/2018
 * Time: 4:17 PM
 */

namespace App\Http\Controllers\Widgets;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Widgets\BaseDimmer;
use App\Models\Blog;

class BlogsDimmer extends BaseDimmer
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $count = Blog::all()->count();
        $string = trans_choice('Blogs', $count);
        $text = 'You have '.$count.' blogs in your database. Click on button below to view all blogs.';
        return view('voyager::dimmer', array_merge($this->config, [
            'icon'   => 'voyager-bag',
            'title'  => "{$count} {$string}",
            'text'   => __($text),
            'button' => [
                'text' => __('View all blogs'),
                'link' => url('/admin/blogs'),
            ],
            'image' => asset('images/Widgets/blog.jpg'),
        ]));
    }

    /**
     * Determine if the widget should be displayed.
     *
     * @return bool
     */
    public function shouldBeDisplayed()
    {
        return Auth::user()->can('browse', Voyager::model('User'));
    }
}