<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'posts';

    protected $fillable = ['title','seo_title','excerpt','body','image','status'];

    public static function store($request,$imagename){
        $item = new Post();
        $item->status = 'PUBLISHED';
        $item->slug = str_replace(' ', '_', mb_strtolower($request->title));
        if($request->title){
            $item->title = $request->title;
        }
        if($request->author_id){
            $item->author_id = $request->author_id;
        }
        if($request->seo_title){
            $item->seo_title = $request->seo_title;
        }
        if($request->excerpt){
            $item->excerpt = $request->excerpt;
        }
        if($request->body){
            $item->body = $request->body;
        }
        if($imagename){
            $item->image = $imagename;
        }
        $item->save();
        return $item;
    }
}
