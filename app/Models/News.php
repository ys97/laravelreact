<?php

namespace App\Models;

use TCG\Voyager\Traits\Resizable;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    //
    use Resizable;
    protected $fillable = ['title','description','image','author_id'];

    public static function store($request,$imagename){
        $item = new News();
        if($request->title){
            $item->title = $request->title;
        }
        if($request->author_id){
            $item->author_id = $request->author_id;
        }
        if($request->description){
            $item->description = $request->description;
        }
        if($imagename){
            $item->image = $imagename;
        }
        $item->save();
        return $item;
    }
}
