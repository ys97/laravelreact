<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $table = 'blogs';
    protected $fillable = ['title','body','image','author_id'];

    public static function store($request,$imagename){
        $item = new Blog();
        if($request->title){
            $item->title = $request->title;
        }
        if($request->author_id){
            $item->author_id = $request->author_id;
        }
        if($request->body){
            $item->body = $request->body;
        }
        if($imagename){
            $item->image = $imagename;
        }
        $item->save();
        return $item;
    }
}
