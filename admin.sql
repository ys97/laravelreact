-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Дек 26 2018 г., 16:14
-- Версия сервера: 5.7.19
-- Версия PHP: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `admin`
--

-- --------------------------------------------------------

--
-- Структура таблицы `blogs`
--

CREATE TABLE `blogs` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'blogs\\December2018\\rxm0XDnqpy5RXMnmYYaN.png',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '0',
  `author_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `blogs`
--

INSERT INTO `blogs` (`id`, `title`, `body`, `image`, `created_at`, `updated_at`, `order`, `author_id`) VALUES
(1, 'Blog 1', 'Blog Body', 'blogs\\December2018\\post1.jpg', '2018-12-05 09:09:00', '2018-12-06 06:12:43', 2, 1),
(2, 'Blog 2', 'dscs svcdsc sd sdc sd s', 'blogs\\December2018\\post2.jpg', '2018-12-05 09:57:00', '2018-12-06 06:25:40', 3, 1),
(3, 'Blog 3', 'das dsa d asdsada', 'blogs\\December2018\\post3.jpg', '2018-12-05 09:58:00', '2018-12-06 06:25:06', 4, 1),
(4, 'Blog', 'asd sa da a da s', 'blogs\\December2018\\post4.jpg', '2018-12-05 10:22:00', '2018-12-13 06:09:43', 1, 1),
(5, 'Blog 4', 'asd sad ad as', 'blogs\\December2018\\post3.jpg', '2018-12-05 10:22:00', '2018-12-10 09:46:11', 5, 1),
(6, 'Blog 5', 'd ad ad asdasdas d', 'blogs\\December2018\\post1.jpg', '2018-12-05 10:22:00', '2018-12-10 09:46:12', 6, 1),
(7, 'Blog 6', 'fsdfd sf sdfds fs', 'blogs\\December2018\\post4.jpg', '2018-12-05 10:23:00', '2018-12-10 09:46:12', 7, 1),
(8, 'Blog 2', '12312 3 123', 'blogs\\December2018\\cSd3SEZFRbFAyDWl0NZ0.png', '2018-12-21 06:23:00', '2018-12-26 09:04:49', 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `order`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, NULL, 1, 'Category 1', 'category-1', '2018-12-06 10:42:12', '2018-12-06 10:42:12'),
(2, NULL, 1, 'Category 2', 'category-2', '2018-12-06 10:42:12', '2018-12-06 10:42:12');

-- --------------------------------------------------------

--
-- Структура таблицы `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, NULL, 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, NULL, 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, NULL, 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, NULL, 8),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":0}', 10),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(11, 1, 'locale', 'text', 'Locale', 0, 1, 1, 1, 1, 0, NULL, 12),
(12, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, NULL, 12),
(13, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(14, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(15, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(16, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(17, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(18, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(19, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(20, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(21, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, NULL, 5),
(22, 1, 'role_id', 'text', 'Role', 1, 1, 1, 1, 1, 1, NULL, 9),
(23, 5, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(24, 5, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '{}', 3),
(25, 5, 'body', 'text_area', 'Body', 0, 1, 1, 1, 1, 1, '{}', 4),
(26, 5, 'image', 'image', 'Image', 1, 1, 1, 1, 1, 1, '{\"resize\":{\"width\":\"1000\",\"height\":\"null\"},\"quality\":\"70%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"300\",\"height\":\"250\"}}]}', 5),
(27, 5, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 6),
(28, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(29, 5, 'order', 'number', 'Order', 1, 1, 1, 1, 0, 1, '{}', 2),
(30, 6, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(31, 6, 'parent_id', 'select_dropdown', 'Parent', 0, 0, 1, 1, 1, 1, '{\"default\":\"\",\"null\":\"\",\"options\":{\"\":\"-- None --\"},\"relationship\":{\"key\":\"id\",\"label\":\"name\"}}', 2),
(32, 6, 'order', 'text', 'Order', 1, 1, 1, 1, 1, 1, '{\"default\":1}', 3),
(33, 6, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 4),
(34, 6, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"name\"}}', 5),
(35, 6, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 0, NULL, 6),
(36, 6, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(37, 7, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '{}', 1),
(38, 7, 'author_id', 'text', 'Author', 1, 0, 1, 1, 0, 1, '{}', 2),
(39, 7, 'category_id', 'text', 'Category', 0, 0, 1, 1, 1, 0, '{}', 3),
(40, 7, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '{}', 4),
(41, 7, 'excerpt', 'text_area', 'Excerpt', 0, 0, 1, 1, 1, 1, '{}', 5),
(42, 7, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, '{}', 6),
(43, 7, 'image', 'image', 'Post Image', 0, 1, 1, 1, 1, 1, '{\"resize\":{\"width\":\"1000\",\"height\":\"null\"},\"quality\":\"70%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"300\",\"height\":\"250\"}}]}', 7),
(44, 7, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true},\"validation\":{\"rule\":\"unique:posts,slug\"}}', 8),
(45, 7, 'meta_description', 'text_area', 'Meta Description', 0, 0, 1, 1, 1, 1, '{}', 9),
(46, 7, 'meta_keywords', 'text_area', 'Meta Keywords', 0, 0, 1, 1, 1, 1, '{}', 10),
(47, 7, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"DRAFT\",\"options\":{\"PUBLISHED\":\"published\",\"DRAFT\":\"draft\",\"PENDING\":\"pending\"}}', 11),
(48, 7, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, '{}', 12),
(49, 7, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 13),
(50, 7, 'seo_title', 'text', 'SEO Title', 0, 1, 1, 1, 1, 1, '{}', 14),
(51, 7, 'featured', 'checkbox', 'Featured', 1, 1, 1, 1, 1, 1, '{}', 15),
(52, 8, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(53, 8, 'author_id', 'text', 'Author', 1, 0, 0, 0, 0, 0, NULL, 2),
(54, 8, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, NULL, 3),
(55, 8, 'excerpt', 'text_area', 'Excerpt', 1, 0, 1, 1, 1, 1, NULL, 4),
(56, 8, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, NULL, 5),
(57, 8, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\"},\"validation\":{\"rule\":\"unique:pages,slug\"}}', 6),
(58, 8, 'meta_description', 'text', 'Meta Description', 1, 0, 1, 1, 1, 1, NULL, 7),
(59, 8, 'meta_keywords', 'text', 'Meta Keywords', 1, 0, 1, 1, 1, 1, NULL, 8),
(60, 8, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"INACTIVE\",\"options\":{\"INACTIVE\":\"INACTIVE\",\"ACTIVE\":\"ACTIVE\"}}', 9),
(61, 8, 'created_at', 'timestamp', 'Created At', 1, 1, 1, 0, 0, 0, NULL, 10),
(62, 8, 'updated_at', 'timestamp', 'Updated At', 1, 0, 0, 0, 0, 0, NULL, 11),
(63, 8, 'image', 'image', 'Page Image', 0, 1, 1, 1, 1, 1, NULL, 12),
(64, 10, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(65, 10, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '{}', 2),
(66, 10, 'description', 'text_area', 'Description', 1, 1, 1, 1, 1, 1, '{}', 3),
(67, 10, 'order', 'number', 'Order', 1, 1, 1, 1, 0, 1, '{}', 4),
(68, 10, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{\"resize\":{\"width\":\"1000\",\"height\":\"null\"},\"quality\":\"70%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"300\",\"height\":\"250\"}}]}', 5),
(69, 10, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 6),
(70, 10, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(71, 7, 'order', 'text', 'Order', 1, 1, 1, 1, 1, 1, '{}', 16);

-- --------------------------------------------------------

--
-- Структура таблицы `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', '', '', 1, 0, NULL, '2018-12-05 08:04:45', '2018-12-05 08:04:45'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2018-12-05 08:04:45', '2018-12-05 08:04:45'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, '', '', 1, 0, NULL, '2018-12-05 08:04:45', '2018-12-05 08:04:45'),
(5, 'blogs', 'blogs', 'Blog', 'Blogs', NULL, 'App\\Models\\Blog', NULL, NULL, NULL, 1, 0, '{\"order_column\":\"order\",\"order_display_column\":\"title\"}', '2018-12-05 08:32:18', '2018-12-11 09:26:19'),
(6, 'categories', 'categories', 'Category', 'Categories', 'voyager-categories', 'TCG\\Voyager\\Models\\Category', NULL, '', '', 1, 0, NULL, '2018-12-06 10:42:12', '2018-12-06 10:42:12'),
(7, 'posts', 'posts', 'Post', 'Posts', 'voyager-news', 'TCG\\Voyager\\Models\\Post', 'TCG\\Voyager\\Policies\\PostPolicy', NULL, NULL, 1, 0, '{\"order_column\":\"order\",\"order_display_column\":\"title\"}', '2018-12-06 10:42:42', '2018-12-26 09:01:38'),
(8, 'pages', 'pages', 'Page', 'Pages', 'voyager-file-text', 'TCG\\Voyager\\Models\\Page', NULL, '', '', 1, 0, NULL, '2018-12-06 10:42:42', '2018-12-06 10:42:42'),
(10, 'news', 'news', 'News', 'News', 'voyager-receipt', 'App\\Models\\News', NULL, NULL, NULL, 1, 0, '{\"order_column\":\"order\",\"order_display_column\":\"title\"}', '2018-12-10 06:26:39', '2018-12-11 09:26:38');

-- --------------------------------------------------------

--
-- Структура таблицы `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2018-12-05 08:04:45', '2018-12-05 08:04:45'),
(2, 'Main', '2018-12-10 06:33:01', '2018-12-10 06:33:01');

-- --------------------------------------------------------

--
-- Структура таблицы `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2018-12-05 08:04:45', '2018-12-05 08:04:45', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 4, '2018-12-05 08:04:45', '2018-12-05 08:42:04', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 3, '2018-12-05 08:04:45', '2018-12-05 08:04:45', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 2, '2018-12-05 08:04:45', '2018-12-05 08:04:45', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 10, '2018-12-05 08:04:45', '2018-12-10 06:27:22', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 1, '2018-12-05 08:04:45', '2018-12-05 08:42:04', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 2, '2018-12-05 08:04:45', '2018-12-05 08:42:04', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 3, '2018-12-05 08:04:45', '2018-12-05 08:42:04', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 4, '2018-12-05 08:04:45', '2018-12-05 08:42:04', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 11, '2018-12-05 08:04:45', '2018-12-10 06:27:22', 'voyager.settings.index', NULL),
(11, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 5, '2018-12-05 08:04:45', '2018-12-05 08:42:04', 'voyager.hooks', NULL),
(12, 1, 'Blogs', '', '_self', 'voyager-bag', '#000000', NULL, 5, '2018-12-05 08:32:18', '2018-12-05 08:42:16', 'voyager.blogs.index', 'null'),
(13, 1, 'Categories', '', '_self', 'voyager-categories', NULL, NULL, 9, '2018-12-06 10:42:12', '2018-12-10 06:27:22', 'voyager.categories.index', NULL),
(14, 1, 'Posts', '', '_self', 'voyager-news', NULL, NULL, 8, '2018-12-06 10:42:42', '2018-12-10 06:27:20', 'voyager.posts.index', NULL),
(15, 1, 'Pages', '', '_self', 'voyager-file-text', NULL, NULL, 7, '2018-12-06 10:42:42', '2018-12-10 06:27:17', 'voyager.pages.index', NULL),
(16, 1, 'News', '', '_self', 'voyager-receipt', NULL, NULL, 6, '2018-12-10 06:26:39', '2018-12-10 06:27:14', 'voyager.news.index', NULL),
(17, 2, 'Home', '/', '_self', NULL, '#000000', NULL, 1, '2018-12-10 06:33:38', '2018-12-10 06:48:18', NULL, ''),
(18, 2, 'Blog', '/blog', '_self', NULL, '#000000', NULL, 3, '2018-12-10 06:48:04', '2018-12-10 06:48:20', NULL, ''),
(19, 2, 'Posts', '/posts', '_self', NULL, '#000000', NULL, 2, '2018-12-10 06:48:16', '2018-12-10 06:48:20', NULL, ''),
(20, 2, 'News', '/news', '_self', NULL, '#000000', NULL, 12, '2018-12-10 08:29:08', '2018-12-10 08:29:08', NULL, '');

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_05_19_173453_create_menu_table', 1),
(6, '2016_10_21_190000_create_roles_table', 1),
(7, '2016_10_21_190000_create_settings_table', 1),
(8, '2016_11_30_135954_create_permission_table', 1),
(9, '2016_11_30_141208_create_permission_role_table', 1),
(10, '2016_12_26_201236_data_types__add__server_side', 1),
(11, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(12, '2017_01_14_005015_create_translations_table', 1),
(13, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(14, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(15, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(16, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(17, '2017_08_05_000000_add_group_to_settings_table', 1),
(18, '2017_11_26_013050_add_user_role_relationship', 1),
(19, '2017_11_26_015000_create_user_roles_table', 1),
(20, '2018_03_11_000000_add_user_settings', 1),
(21, '2018_03_14_000000_add_details_to_data_types_table', 1),
(22, '2018_03_16_000000_make_settings_value_nullable', 1),
(23, '2018_12_05_122905_create_blogs_table', 2),
(24, '2018_12_05_141837_add_order_column_to_blogs_table', 3),
(25, '2016_01_01_000000_create_pages_table', 4),
(26, '2016_01_01_000000_create_posts_table', 4),
(27, '2016_02_15_204651_create_categories_table', 4),
(28, '2017_04_11_000000_alter_post_nullable_fields_table', 4),
(30, '2018_12_10_101509_create_news_table', 5),
(31, '2018_12_21_125449_add_author_id_to_news_table', 6),
(32, '2018_12_21_125455_add_author_id_to_blogs_table', 6),
(34, '2018_12_26_110139_add_order_to_posts_table', 7);

-- --------------------------------------------------------

--
-- Структура таблицы `news`
--

CREATE TABLE `news` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '0',
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `author_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `news`
--

INSERT INTO `news` (`id`, `title`, `description`, `order`, `image`, `created_at`, `updated_at`, `author_id`) VALUES
(1, 'News 1', 'Desc', 8, 'news\\December2018\\cjWk4A5dnfVfFMu6KzWI.jpg', '2018-12-10 06:27:00', '2018-12-26 06:48:20', 1),
(2, 'News 2', 'sd fsd fds fds fsd fsdfsdf', 1, 'news\\December2018\\HdpGINqAEhAjyo6kNgAE.jpg', '2018-12-10 08:44:24', '2018-12-26 06:48:20', 1),
(3, 'News 3', 'fsd fsdkjla dasd asdasd asd jlaskdj lasjda sdjasdklasdjlasj asd asd s', 7, 'news\\December2018\\ueDU0lne3Zw2YRi5scgp.jpg', '2018-12-10 08:44:00', '2018-12-26 06:48:20', 1),
(4, 'News 4', 'asd sadlkasj das dvn cxm,vn e lqkej', 6, 'news\\December2018\\yMbwjJkN0Gu94668wvwu.jpg', '2018-12-10 08:45:00', '2018-12-26 06:48:20', 1),
(5, 'News 5', 'a sddj sajd  asd  jlkj m', 3, 'news\\December2018\\vFkGghJUtj2GraNaSXqR.jpg', '2018-12-10 08:45:00', '2018-12-26 06:49:00', 1),
(11, 'asdsadsa', 'dsa dsa dasdas', 4, 'news\\December2018\\GM6rJylJ3FijaaHtJDXp.jpg', '2018-12-21 09:09:00', '2018-12-26 06:49:19', 11),
(15, 'asdasdasd', 'asdsdasdasdas', 2, 'news\\December2018\\gsPpSXawDtXch9KytliJ.png', '2018-12-24 07:39:00', '2018-12-26 06:48:20', 11),
(16, 'asdsadasdsadas', 'dasdsaddasdasdasd', 5, 'news/86286.png', '2018-12-24 07:50:22', '2018-12-26 06:49:00', 11);

-- --------------------------------------------------------

--
-- Структура таблицы `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `pages`
--

INSERT INTO `pages` (`id`, `author_id`, `title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, 'Hello World', 'Hang the jib grog grog blossom grapple dance the hempen jig gangway pressgang bilge rat to go on account lugger. Nelsons folly gabion line draught scallywag fire ship gaff fluke fathom case shot. Sea Legs bilge rat sloop matey gabion long clothes run a shot across the bow Gold Road cog league.', '<p>Hello World. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', 'pages/page1.jpg', 'hello-world', 'Yar Meta Description', 'Keyword1, Keyword2', 'ACTIVE', '2018-12-06 10:42:42', '2018-12-06 10:42:42');

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2018-12-05 08:04:45', '2018-12-05 08:04:45'),
(2, 'browse_bread', NULL, '2018-12-05 08:04:45', '2018-12-05 08:04:45'),
(3, 'browse_database', NULL, '2018-12-05 08:04:45', '2018-12-05 08:04:45'),
(4, 'browse_media', NULL, '2018-12-05 08:04:45', '2018-12-05 08:04:45'),
(5, 'browse_compass', NULL, '2018-12-05 08:04:45', '2018-12-05 08:04:45'),
(6, 'browse_menus', 'menus', '2018-12-05 08:04:45', '2018-12-05 08:04:45'),
(7, 'read_menus', 'menus', '2018-12-05 08:04:45', '2018-12-05 08:04:45'),
(8, 'edit_menus', 'menus', '2018-12-05 08:04:45', '2018-12-05 08:04:45'),
(9, 'add_menus', 'menus', '2018-12-05 08:04:45', '2018-12-05 08:04:45'),
(10, 'delete_menus', 'menus', '2018-12-05 08:04:45', '2018-12-05 08:04:45'),
(11, 'browse_roles', 'roles', '2018-12-05 08:04:45', '2018-12-05 08:04:45'),
(12, 'read_roles', 'roles', '2018-12-05 08:04:45', '2018-12-05 08:04:45'),
(13, 'edit_roles', 'roles', '2018-12-05 08:04:45', '2018-12-05 08:04:45'),
(14, 'add_roles', 'roles', '2018-12-05 08:04:45', '2018-12-05 08:04:45'),
(15, 'delete_roles', 'roles', '2018-12-05 08:04:45', '2018-12-05 08:04:45'),
(16, 'browse_users', 'users', '2018-12-05 08:04:45', '2018-12-05 08:04:45'),
(17, 'read_users', 'users', '2018-12-05 08:04:45', '2018-12-05 08:04:45'),
(18, 'edit_users', 'users', '2018-12-05 08:04:45', '2018-12-05 08:04:45'),
(19, 'add_users', 'users', '2018-12-05 08:04:45', '2018-12-05 08:04:45'),
(20, 'delete_users', 'users', '2018-12-05 08:04:45', '2018-12-05 08:04:45'),
(21, 'browse_settings', 'settings', '2018-12-05 08:04:45', '2018-12-05 08:04:45'),
(22, 'read_settings', 'settings', '2018-12-05 08:04:45', '2018-12-05 08:04:45'),
(23, 'edit_settings', 'settings', '2018-12-05 08:04:45', '2018-12-05 08:04:45'),
(24, 'add_settings', 'settings', '2018-12-05 08:04:45', '2018-12-05 08:04:45'),
(25, 'delete_settings', 'settings', '2018-12-05 08:04:45', '2018-12-05 08:04:45'),
(26, 'browse_hooks', NULL, '2018-12-05 08:04:45', '2018-12-05 08:04:45'),
(27, 'browse_blogs', 'blogs', '2018-12-05 08:32:18', '2018-12-05 08:32:18'),
(28, 'read_blogs', 'blogs', '2018-12-05 08:32:18', '2018-12-05 08:32:18'),
(29, 'edit_blogs', 'blogs', '2018-12-05 08:32:18', '2018-12-05 08:32:18'),
(30, 'add_blogs', 'blogs', '2018-12-05 08:32:18', '2018-12-05 08:32:18'),
(31, 'delete_blogs', 'blogs', '2018-12-05 08:32:18', '2018-12-05 08:32:18'),
(32, 'browse_categories', 'categories', '2018-12-06 10:42:12', '2018-12-06 10:42:12'),
(33, 'read_categories', 'categories', '2018-12-06 10:42:12', '2018-12-06 10:42:12'),
(34, 'edit_categories', 'categories', '2018-12-06 10:42:12', '2018-12-06 10:42:12'),
(35, 'add_categories', 'categories', '2018-12-06 10:42:12', '2018-12-06 10:42:12'),
(36, 'delete_categories', 'categories', '2018-12-06 10:42:12', '2018-12-06 10:42:12'),
(37, 'browse_posts', 'posts', '2018-12-06 10:42:42', '2018-12-06 10:42:42'),
(38, 'read_posts', 'posts', '2018-12-06 10:42:42', '2018-12-06 10:42:42'),
(39, 'edit_posts', 'posts', '2018-12-06 10:42:42', '2018-12-06 10:42:42'),
(40, 'add_posts', 'posts', '2018-12-06 10:42:42', '2018-12-06 10:42:42'),
(41, 'delete_posts', 'posts', '2018-12-06 10:42:42', '2018-12-06 10:42:42'),
(42, 'browse_pages', 'pages', '2018-12-06 10:42:42', '2018-12-06 10:42:42'),
(43, 'read_pages', 'pages', '2018-12-06 10:42:42', '2018-12-06 10:42:42'),
(44, 'edit_pages', 'pages', '2018-12-06 10:42:42', '2018-12-06 10:42:42'),
(45, 'add_pages', 'pages', '2018-12-06 10:42:42', '2018-12-06 10:42:42'),
(46, 'delete_pages', 'pages', '2018-12-06 10:42:42', '2018-12-06 10:42:42'),
(47, 'browse_news', 'news', '2018-12-10 06:26:39', '2018-12-10 06:26:39'),
(48, 'read_news', 'news', '2018-12-10 06:26:39', '2018-12-10 06:26:39'),
(49, 'edit_news', 'news', '2018-12-10 06:26:39', '2018-12-10 06:26:39'),
(50, 'add_news', 'news', '2018-12-10 06:26:39', '2018-12-10 06:26:39'),
(51, 'delete_news', 'news', '2018-12-10 06:26:39', '2018-12-10 06:26:39');

-- --------------------------------------------------------

--
-- Структура таблицы `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 3),
(2, 1),
(3, 1),
(4, 1),
(4, 3),
(5, 1),
(5, 3),
(6, 1),
(6, 3),
(7, 1),
(7, 3),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(11, 3),
(12, 1),
(12, 3),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(16, 3),
(17, 1),
(17, 3),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(27, 3),
(28, 1),
(28, 3),
(29, 1),
(29, 3),
(30, 1),
(30, 3),
(31, 1),
(31, 3),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DRAFT',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `posts`
--

INSERT INTO `posts` (`id`, `author_id`, `category_id`, `title`, `seo_title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `featured`, `created_at`, `updated_at`, `order`) VALUES
(1, 1, 1, 'Lorem Ipsum Post', '', 'This is the excerpt for the Lorem Ipsum Post', '<p>This is the body of the lorem ipsum post</p>', 'posts/post1.jpg', 'lorem-ipsum-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2018-12-06 10:42:42', '2018-12-26 09:02:58', 2),
(2, 1, NULL, 'My Sample Post', NULL, 'This is the excerpt for the sample Post', '<p>This is the body for the sample post, which includes the body.</p>\n                <h2>We can use all kinds of format!</h2>\n                <p>And include a bunch of other stuff.</p>', 'posts/post2.jpg', 'my-sample-post', 'Meta Description for sample post', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2018-12-06 10:42:42', '2018-12-26 09:02:58', 3),
(3, 1, 1, 'Latest Post', '', 'This is the excerpt for the latest post', '<p>This is the body for the latest post</p>', 'posts/post3.jpg', 'latest-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2018-12-06 10:42:42', '2018-12-26 09:02:58', 4),
(4, 1, NULL, 'Yarr Post', NULL, 'Reef sails nipperkin bring a spring upon her cable coffer jury mast spike marooned Pieces of Eight poop deck pillage. Clipper driver coxswain galleon hempen halter come about pressgang gangplank boatswain swing the lead. Nipperkin yard skysail swab lanyard Blimey bilge water ho quarter Buccaneer.', '<p>Swab deadlights Buccaneer fire ship square-rigged dance the hempen jig weigh anchor cackle fruit grog furl. Crack Jennys tea cup chase guns pressgang hearties spirits hogshead Gold Road six pounders fathom measured fer yer chains. Main sheet provost come about trysail barkadeer crimp scuttle mizzenmast brig plunder.</p>\n<p>Mizzen league keelhaul galleon tender cog chase Barbary Coast doubloon crack Jennys tea cup. Blow the man down lugsail fire ship pinnace cackle fruit line warp Admiral of the Black strike colors doubloon. Tackle Jack Ketch come about crimp rum draft scuppers run a shot across the bow haul wind maroon.</p>\n<p>Interloper heave down list driver pressgang holystone scuppers tackle scallywag bilged on her anchor. Jack Tar interloper draught grapple mizzenmast hulk knave cable transom hogshead. Gaff pillage to go on account grog aft chase guns piracy yardarm knave clap of thunder.</p>', 'posts/post4.jpg', 'yarr-post', 'this be a meta descript', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2018-12-06 10:42:42', '2018-12-26 09:02:58', 5),
(16, 11, 1, 'dsAagdfgdfgd asd as d', NULL, 'fsdffdas as dsa das', '<p>fsdfsdfsdffasd asd as</p>', 'posts\\December2018\\TNW0JmTsiLl2CBcJI6EO.jpg', 'dsaa', NULL, NULL, 'PUBLISHED', 0, '2018-12-24 06:36:33', '2018-12-26 09:02:32', 6),
(26, 11, 1, 'asdasd', NULL, 'asdasda', '<p>sdasdasdasdada</p>', 'posts\\December2018\\eJtu1p8zIvii75RbAIjw.jpg', 'asdasd', NULL, NULL, 'PUBLISHED', 0, '2018-12-24 07:39:00', '2018-12-26 09:03:03', 7),
(30, 11, NULL, 'ad', NULL, 'dasdasd', 'dasdasd', 'posts/47900.png', 'ad', NULL, NULL, 'PUBLISHED', 0, '2018-12-24 07:42:06', '2018-12-26 09:02:34', 9),
(35, 11, 1, 'dddddddddaaaaaaaaaaaa', NULL, 'aaaaaaaaaaaaa', '<p>aaaaaaaaaaaaaaaaaaaaaaaa</p>', 'posts\\December2018\\2YeNJXyaBMJddp4CwGgX.png', 'dddddddddaaaaaaaaaaaa', NULL, NULL, 'PUBLISHED', 0, '2018-12-24 07:50:01', '2018-12-26 09:02:32', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2018-12-05 08:04:45', '2018-12-05 08:04:45'),
(2, 'user', 'Normal User', '2018-12-05 08:04:45', '2018-12-05 08:04:45'),
(3, 'partner', 'Partner', '2018-12-06 10:32:00', '2018-12-06 10:32:00');

-- --------------------------------------------------------

--
-- Структура таблицы `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'React Laravel', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Welcome to React-Laravel', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', 'settings\\December2018\\9GWxofq60ida2ngYJBcL.png', '', 'image', 3, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', 'settings\\December2018\\cNMLIEBp7madxj0NWtoW.jpg', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'Admin', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Welcome to React-Laravel', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', 'settings\\December2018\\hyQN0xrRUgbfBtWko56W.png', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', 'settings\\December2018\\Jw8owdmJdprj5Oz4KGiQ.png', '', 'image', 4, 'Admin');

-- --------------------------------------------------------

--
-- Структура таблицы `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `translations`
--

INSERT INTO `translations` (`id`, `table_name`, `column_name`, `foreign_key`, `locale`, `value`, `created_at`, `updated_at`) VALUES
(1, 'data_types', 'display_name_singular', 7, 'pt', 'Post', '2018-12-06 10:42:42', '2018-12-06 10:42:42'),
(2, 'data_types', 'display_name_singular', 8, 'pt', 'Página', '2018-12-06 10:42:42', '2018-12-06 10:42:42'),
(3, 'data_types', 'display_name_singular', 1, 'pt', 'Utilizador', '2018-12-06 10:42:42', '2018-12-06 10:42:42'),
(4, 'data_types', 'display_name_singular', 6, 'pt', 'Categoria', '2018-12-06 10:42:42', '2018-12-06 10:42:42'),
(5, 'data_types', 'display_name_singular', 2, 'pt', 'Menu', '2018-12-06 10:42:42', '2018-12-06 10:42:42'),
(6, 'data_types', 'display_name_singular', 3, 'pt', 'Função', '2018-12-06 10:42:42', '2018-12-06 10:42:42'),
(7, 'data_types', 'display_name_plural', 7, 'pt', 'Posts', '2018-12-06 10:42:42', '2018-12-06 10:42:42'),
(8, 'data_types', 'display_name_plural', 8, 'pt', 'Páginas', '2018-12-06 10:42:42', '2018-12-06 10:42:42'),
(9, 'data_types', 'display_name_plural', 1, 'pt', 'Utilizadores', '2018-12-06 10:42:42', '2018-12-06 10:42:42'),
(10, 'data_types', 'display_name_plural', 6, 'pt', 'Categorias', '2018-12-06 10:42:42', '2018-12-06 10:42:42'),
(11, 'data_types', 'display_name_plural', 2, 'pt', 'Menus', '2018-12-06 10:42:42', '2018-12-06 10:42:42'),
(12, 'data_types', 'display_name_plural', 3, 'pt', 'Funções', '2018-12-06 10:42:42', '2018-12-06 10:42:42'),
(13, 'categories', 'slug', 1, 'pt', 'categoria-1', '2018-12-06 10:42:42', '2018-12-06 10:42:42'),
(14, 'categories', 'name', 1, 'pt', 'Categoria 1', '2018-12-06 10:42:42', '2018-12-06 10:42:42'),
(15, 'categories', 'slug', 2, 'pt', 'categoria-2', '2018-12-06 10:42:42', '2018-12-06 10:42:42'),
(16, 'categories', 'name', 2, 'pt', 'Categoria 2', '2018-12-06 10:42:42', '2018-12-06 10:42:42'),
(17, 'pages', 'title', 1, 'pt', 'Olá Mundo', '2018-12-06 10:42:42', '2018-12-06 10:42:42'),
(18, 'pages', 'slug', 1, 'pt', 'ola-mundo', '2018-12-06 10:42:42', '2018-12-06 10:42:42'),
(19, 'pages', 'body', 1, 'pt', '<p>Olá Mundo. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', '2018-12-06 10:42:42', '2018-12-06 10:42:42'),
(20, 'menu_items', 'title', 1, 'pt', 'Painel de Controle', '2018-12-06 10:42:42', '2018-12-06 10:42:42'),
(21, 'menu_items', 'title', 2, 'pt', 'Media', '2018-12-06 10:42:42', '2018-12-06 10:42:42'),
(22, 'menu_items', 'title', 14, 'pt', 'Publicações', '2018-12-06 10:42:42', '2018-12-06 10:42:42'),
(23, 'menu_items', 'title', 3, 'pt', 'Utilizadores', '2018-12-06 10:42:42', '2018-12-06 10:42:42'),
(24, 'menu_items', 'title', 13, 'pt', 'Categorias', '2018-12-06 10:42:42', '2018-12-06 10:42:42'),
(25, 'menu_items', 'title', 15, 'pt', 'Páginas', '2018-12-06 10:42:42', '2018-12-06 10:42:42'),
(26, 'menu_items', 'title', 4, 'pt', 'Funções', '2018-12-06 10:42:42', '2018-12-06 10:42:42'),
(27, 'menu_items', 'title', 5, 'pt', 'Ferramentas', '2018-12-06 10:42:42', '2018-12-06 10:42:42'),
(28, 'menu_items', 'title', 6, 'pt', 'Menus', '2018-12-06 10:42:42', '2018-12-06 10:42:42'),
(29, 'menu_items', 'title', 7, 'pt', 'Base de dados', '2018-12-06 10:42:42', '2018-12-06 10:42:42'),
(30, 'menu_items', 'title', 10, 'pt', 'Configurações', '2018-12-06 10:42:42', '2018-12-06 10:42:42'),
(31, 'blogs', 'title', 7, 'hy', 'Blog 6', '2018-12-12 06:46:37', '2018-12-12 06:46:37'),
(32, 'blogs', 'body', 7, 'hy', 'fsdfd sf sdfds fs arm', '2018-12-12 06:46:37', '2018-12-12 06:46:37'),
(33, 'blogs', 'title', 4, 'hy', 'Hyyyyyyyyyyyyyy', '2018-12-13 06:09:43', '2018-12-13 06:09:43'),
(34, 'blogs', 'body', 4, 'hy', 'asd sa da a da s', '2018-12-13 06:09:43', '2018-12-13 06:09:43'),
(35, 'blogs', 'title', 1, 'hy', 'AAArrrr', '2018-12-13 06:19:30', '2018-12-13 06:19:30'),
(36, 'blogs', 'body', 1, 'hy', 'Blog Body', '2018-12-13 06:19:30', '2018-12-13 06:19:30'),
(37, 'posts', 'title', 1, 'hy', 'Arm Lorem Ipsum Post', '2018-12-13 09:04:33', '2018-12-13 09:04:33'),
(38, 'posts', 'seo_title', 1, 'hy', '', '2018-12-13 09:04:33', '2018-12-13 09:04:33'),
(39, 'posts', 'excerpt', 1, 'hy', 'This is the excerpt for the Lorem Ipsum Post', '2018-12-13 09:04:33', '2018-12-13 09:04:33'),
(40, 'posts', 'body', 1, 'hy', '<p>This is the body of the lorem ipsum post</p>', '2018-12-13 09:04:33', '2018-12-13 09:04:33'),
(41, 'posts', 'slug', 1, 'hy', 'arm-lorem-ipsum-post', '2018-12-13 09:04:33', '2018-12-13 09:04:33'),
(42, 'posts', 'meta_description', 1, 'hy', 'This is the meta description', '2018-12-13 09:04:33', '2018-12-13 09:04:33'),
(43, 'posts', 'meta_keywords', 1, 'hy', 'keyword1, keyword2, keyword3', '2018-12-13 09:04:33', '2018-12-13 09:04:33'),
(44, 'posts', 'title', 3, 'hy', 'Arm Latest Post', '2018-12-13 09:04:43', '2018-12-13 09:05:55'),
(45, 'posts', 'seo_title', 3, 'hy', '', '2018-12-13 09:04:43', '2018-12-13 09:04:43'),
(46, 'posts', 'excerpt', 3, 'hy', 'This is the excerpt for the latest post', '2018-12-13 09:04:43', '2018-12-13 09:04:43'),
(47, 'posts', 'body', 3, 'hy', '<p>This is the body for the latest post</p>', '2018-12-13 09:04:43', '2018-12-13 09:04:43'),
(48, 'posts', 'slug', 3, 'hy', 'arm-latest-post', '2018-12-13 09:04:43', '2018-12-13 09:05:55'),
(49, 'posts', 'meta_description', 3, 'hy', 'This is the meta description', '2018-12-13 09:04:43', '2018-12-13 09:04:43'),
(50, 'posts', 'meta_keywords', 3, 'hy', 'keyword1, keyword2, keyword3', '2018-12-13 09:04:43', '2018-12-13 09:04:43'),
(51, 'news', 'title', 4, 'hy', 'Ar', '2018-12-13 09:15:36', '2018-12-13 09:15:36'),
(52, 'news', 'description', 4, 'hy', 'arrrrrrrrrrrrrrrrrrrrrr', '2018-12-13 09:15:36', '2018-12-13 09:15:36'),
(53, 'news', 'title', 3, 'hy', 'Ar 3', '2018-12-13 09:16:10', '2018-12-13 09:16:10'),
(54, 'news', 'description', 3, 'hy', 'Arasd ad a', '2018-12-13 09:16:10', '2018-12-13 09:16:10');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`) VALUES
(1, 1, 'admin', 'admin@admin.com', 'users/default.png', NULL, '$2y$10$SD.5nKHtQCFqRD67FY0D1uq2gPvxQ0Fj3UKA02kHDn8/dudpEb0gS', 'HPANdGKKVvPBs7l6tTcjZAyxPqSH9yM5xGAxiyVRVeywggdDDwaApp9fa4Wt', '{\"locale\":\"en\"}', '2018-12-02 20:00:00', '2018-12-06 10:52:21'),
(2, 3, 'Partner', 'partner@admin.com', 'users/default.png', NULL, '$2y$10$b1B.ynpZDPdfe0qMxfXTme/byMM6PFg5nSyBhCsLHuEiuRrAht8Uy', 'tBxg8jM4IR21IbX28MCfil3JWtAmBPOApIcYQVlcxinAjmq2sMY8YwwkQ3Ve', '{\"locale\":\"en\"}', '2018-12-06 10:32:29', '2018-12-06 10:32:29'),
(4, 3, 'admin', 'admin@partner.com', 'users/default.png', NULL, '$2y$10$Dz8SXLpwrIJttzfixckOge67F3A2L5ssQUtA8UM0SSD5AJaaj5W8C', NULL, NULL, NULL, NULL),
(11, 2, 'User ', 'user@user.com', 'users/default.png', NULL, '$2y$10$XmOls/qLLK8UMOocaUurFeh0I2xXwwWhA.D8eIAUVh0b/tLhMe.qy', 'sx1oH9OaulILPawWhsZtDJbbQzly7zIIidmBhs2ywkOWPdNY3mxcvetJ66M1', NULL, '2018-12-20 10:39:49', '2018-12-20 10:39:49'),
(12, 2, 'User 2', 'user@user1.com', 'users/default.png', NULL, '$2y$10$pRsZa7Pu5IwVSWMcYu5p4uGk6J3dXJDHGyTo4EYBN77XnqRscsRxu', NULL, NULL, '2018-12-21 04:29:18', '2018-12-21 04:29:18'),
(13, 2, 'yero', 'y@m.ru', 'users/default.png', NULL, '$2y$10$J3viyJ0Vgrs27EapHc2piOibNtbZ7NERygH5NeFieLlN9NrkFu3fW', NULL, NULL, '2018-12-25 07:07:48', '2018-12-25 07:07:48'),
(14, 2, 'Yero', 'user@user2.com', 'users/default.png', NULL, '$2y$10$DO/LyvW6I3UMjxr4C7cEm.12U9p5TjgT5APeMfVA98jwmO6Dk/33i', NULL, NULL, '2018-12-25 07:47:22', '2018-12-25 07:47:22');

-- --------------------------------------------------------

--
-- Структура таблицы `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`),
  ADD KEY `categories_parent_id_foreign` (`parent_id`);

--
-- Индексы таблицы `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Индексы таблицы `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Индексы таблицы `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Индексы таблицы `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pages_slug_unique` (`slug`);

--
-- Индексы таблицы `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Индексы таблицы `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Индексы таблицы `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Индексы таблицы `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `posts_slug_unique` (`slug`);

--
-- Индексы таблицы `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Индексы таблицы `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Индексы таблицы `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Индексы таблицы `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT для таблицы `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;
--
-- AUTO_INCREMENT для таблицы `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT для таблицы `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT для таблицы `news`
--
ALTER TABLE `news`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT для таблицы `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT для таблицы `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT для таблицы `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT для таблицы `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Ограничения внешнего ключа таблицы `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
