import Login from '../pages/login';
import Register from '../pages/register';
import ForgotPassword from '../pages/forgotPassword';
import ResetPassword from '../pages/resetPassword';
import Dashboard from '../pages/dashboard';
import NoMatch from '../pages/noMatch';
import UserPosts from '../pages/posts';
import EditPosts from '../pages/edit_posts';
import UserBlog from '../pages/myBlog';
import EditBlog from '../pages/edit_blog';
import UserNews from '../pages/myNews';
import EditNews from '../pages/edit_news';
import Home from '../components/Home';
import Blog from '../components/Blog';
import ViewBlog from '../components/ViewBlog';
import Posts from '../components/Posts';
import ViewPost from '../components/ViewPost';
import News from '../components/News';
import ViewNews from '../components/ViewNews';

const routes = [
    {
        path: '/',
        exact: true,
        auth: false,
        component: Home
    },
    {
        path: '/login',
        exact: true,
        auth: false,
        component: Login
    },
    {
        path: '/blog',
        exact: true,
        auth: false,
        component: Blog
    },
    {
        path: '/posts',
        exact: true,
        auth: false,
        component: Posts
    },
    {
        path: '/news',
        exact: true,
        auth: false,
        component: News
    },
    {
        path: '/blog/:id',
        exact: true,
        auth: false,
        component: ViewBlog
    },
    {
        path: '/post/:id',
        exact: true,
        auth: false,
        component: ViewPost
    },
    {
        path: '/news/:id',
        exact: true,
        auth: false,
        component: ViewNews
    },
    {
        path: '/register',
        exact: true,
        auth: false,
        component: Register
    },
    {
        path: '/forgot-password',
        exact: true,
        auth: false,
        component: ForgotPassword
    },
    {
        path: '/reset-password/:token/:email',
        exact: true,
        auth: false,
        component: ResetPassword
    },
    {
        path: '/dashboard',
        exact: true,
        auth: true,
        component: Dashboard
    },
    {
        path: '/my_posts/:id',
        exact: true,
        auth: true,
        component: UserPosts
    },
    {
        path: '/edit/post/:id',
        exact: true,
        auth: true,
        component: EditPosts
    },
    {
        path: '/my_blog/:id',
        exact: true,
        auth: true,
        component: UserBlog
    },
    {
        path: '/edit/blog/:id',
        exact: true,
        auth: true,
        component: EditBlog
    },
    {
        path: '/my_news/:id',
        exact: true,
        auth: true,
        component: UserNews
    },
    {
        path: '/edit/news/:id',
        exact: true,
        auth: true,
        component: EditNews
    },
    {
        path: '',
        exact: true,
        auth: false,
        component: NoMatch
    }
];

export default routes;