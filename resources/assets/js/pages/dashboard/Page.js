import React from 'react'
import {
    Button,
    Container,
    Grid,
    Header,
    Icon,
    Responsive,
    Segment,
    Step
} from 'semantic-ui-react';
import $ from 'jquery';
import 'bootstrap/dist/js/bootstrap.bundle';
import PageHeader from '../../common/pageHeader';
import axios from 'axios';

class Page extends React.Component {
    constructor(props) {
        super(props);
    }

    addNewPosts(event){
        const data = new FormData();
        data.append('image', document.getElementById('post_image').files[0]);
        data.append('title', document.getElementById('post_title').value);
        data.append('excerpt', document.getElementById('excerpt').value);
        data.append('body', document.getElementById('post_body').value);
        data.append('author_id', this.props.id);
        event.preventDefault()
        document.getElementById('post_submit').setAttribute('disabled',true)
        document.getElementById('post_submit').innerHTML = ('Please wait...')
        axios.post('/api/posts',data).then(response => {
            setInterval(function () {
                document.getElementById('post_submit').removeAttribute('disabled')
                document.getElementById('post_submit').innerHTML = ('Submit')
                document.getElementById("posts_form").reset();
            },1000)
        }).catch(error => {
            console.log(error)
            setInterval(function () {
                document.getElementById('post_submit').removeAttribute('disabled')
                document.getElementById('post_submit').innerHTML = ('Try again...')
            },1000)
        })
    }

    addNewBlog(event){
        const data = new FormData();
        data.append('image', document.getElementById('blog_image').files[0]);
        data.append('title', document.getElementById('blog_title').value);
        data.append('body', document.getElementById('blog_body').value);
        data.append('author_id', this.props.id);
        event.preventDefault();
        document.getElementById('blog_submit').setAttribute('disabled',true)
        document.getElementById('blog_submit').innerHTML = ('Please wait...')
        axios.post('/api/blog',data).then(response => {
            setInterval(function () {
                document.getElementById('blog_submit').removeAttribute('disabled')
                document.getElementById('blog_submit').innerHTML = ('Submit')
                document.getElementById("blog_form").reset();
            },1000)
        }).catch(error => {
            console.log(error)
            setInterval(function () {
                document.getElementById('blog_submit').removeAttribute('disabled')
                document.getElementById('blog_submit').innerHTML = ('Try again...')
            },1000)
        })
    }

    addNews(event){
        const data = new FormData();
        data.append('image', document.getElementById('news_image').files[0]);
        data.append('title', document.getElementById('news_title').value);
        data.append('description', document.getElementById('news_description').value);
        data.append('author_id', this.props.id);
        event.preventDefault();
        document.getElementById('news_submit').setAttribute('disabled',true)
        document.getElementById('news_submit').innerHTML = ('Please wait...')
        axios.post('/api/news',data).then(response => {
            setInterval(function () {
                document.getElementById('news_submit').removeAttribute('disabled')
                document.getElementById('news_submit').innerHTML = ('Submit')
                document.getElementById("news_form").reset();
            },1000)
        }).catch(error => {
            console.log(error)
            setInterval(function () {
                document.getElementById('news_submit').removeAttribute('disabled')
                document.getElementById('news_submit').innerHTML = ('Try again...')
            },1000)
        })
    }

    render() {
        const userName = this.props.userName;
        const avatar = this.props.avatar;
        const email = this.props.email;
        return (
            <React.Fragment>
                <PageHeader heading="Dashboard"/>
                <div className="container-fluid pt-4">
                    <div className="row">
                        <div className="col-3">
                            <h1>{userName}</h1>
                            <h3>{email}</h3>
                            <img src={'/uploads/'+avatar} className={'img-fluid avatar'} alt=""/>
                        </div>
                        <div className="col-9">
                            <div className="row">
                                <div className="col-sm-12">
                                    <ul className="nav nav-tabs" id="myTab" role="tablist">
                                        <li className="nav-item">
                                            <a className="nav-link text-light active" id="home-tab" data-toggle="tab" href="#posts_tab" role="tab"
                                               aria-controls="home" aria-selected="true">Add new Post</a>
                                        </li>
                                        <li className="nav-item">
                                            <a className="nav-link text-light" id="profile-tab" data-toggle="tab" href="#blog_tab" role="tab"
                                               aria-controls="profile" aria-selected="false">Add new Blog</a>
                                        </li>
                                        <li className="nav-item">
                                            <a className="nav-link text-light" id="news-tab" data-toggle="tab" href="#news_tab" role="tab"
                                               aria-controls="contact" aria-selected="false">Add new News</a>
                                        </li>
                                    </ul>
                                </div>
                                <div className="col-sm-12 pt-5">
                                    <div className="row jcc">
                                        <div className="col-sm-4">
                                            <div className="tab-content" id="myTabContent">
                                                <div className="tab-pane fade show active" id="posts_tab" role="tabpanel" aria-labelledby="home-tab">
                                                    <form onSubmit={this.addNewPosts.bind(this)} id={'posts_form'}>
                                                        <input type="text" name={'post_title'} className={'form-control-plaintext text-light'} placeholder={'Title here...'} id={'post_title'} required autoComplete={'off'}/>
                                                        <div className={'form-group pt-3'}>
                                                            <textarea className={'textarea form-control-plaintext text-light'} name={'excerpt'} placeholder={'Excerpt here...'} id={'excerpt'}></textarea>
                                                        </div>
                                                        <div className={'form-group pt-3'}>
                                                            <textarea className={'textarea form-control-plaintext text-light'} name={'post_body'} placeholder={'Body here...'} id={'post_body'}></textarea>
                                                        </div>
                                                        <div className={'form-group pt-3'}>
                                                            <input type={'file'} name={'post_image'} className={'form-control-plaintext text-light-file'} id={'post_image'} />
                                                        </div>
                                                        <button type={'submit'} className={'mb-5 btn-block btn pr-5 pl-5 btn-outline-danger'} id={'post_submit'}>Submit</button>
                                                    </form>
                                                </div>
                                                <div className="tab-pane fade" id="blog_tab" role="tabpanel" aria-labelledby="profile-tab">
                                                    <form onSubmit={this.addNewBlog.bind(this)} id={'blog_form'}>
                                                        <input type="text" name={'blog_title'} className={'form-control-plaintext text-light'} placeholder={'Title here...'} id={'blog_title'} required/>
                                                        <div className={'form-group pt-3'}>
                                                            <textarea className={'textarea form-control-plaintext text-light'} name={'blog_body'} id={'blog_body'} placeholder={'Body here...'}></textarea>
                                                        </div>
                                                        <div className={'form-group pt-3'}>
                                                            <input type={'file'} name={'blog_image'} className={'form-control-plaintext text-light-file'} id={'blog_image'}/>
                                                        </div>
                                                        <button type={'submit'} className={'mb-5 btn-block btn pr-5 pl-5 btn-outline-danger'} id={'blog_submit'}>Submit</button>
                                                    </form>
                                                </div>
                                                <div className="tab-pane fade" id="news_tab" role="tabpanel" aria-labelledby="news_tab">
                                                    <form onSubmit={this.addNews.bind(this)} id={'news_form'}>
                                                        <input type="text" name={'news_title'} className={'form-control-plaintext text-light'} placeholder={'Title here...'} id={'news_title'} required/>
                                                        <div className={'form-group pt-3'}>
                                                            <textarea className={'textarea form-control-plaintext text-light'} name={'news_body'} placeholder={'Description here...'} id={'news_description'}></textarea>
                                                        </div>
                                                        <div className={'form-group pt-3'}>
                                                            <input type={'file'} name={'news_image'} className={'form-control-plaintext text-light-file'} id={'news_image'} />
                                                        </div>
                                                        <button type={'submit'} className={'mb-5 btn-block btn pr-5 pl-5 btn-outline-danger'} id={'news_submit'}>Submit</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default Page;