import React from 'react'
import 'bootstrap/dist/js/bootstrap.bundle';
import PageHeader from '../../common/pageHeader';
import axios from 'axios';
import {Link} from "react-router-dom";
import '@fortawesome/fontawesome-free/css/all.css';
import Pagination from "react-js-pagination";

class Page extends React.Component {
    constructor(props) {
        super(props);
        const hash = window.location.hash.substring(1)
        this.state = {
            newss:[],
            activePage:1,
            page:hash,
            id:this.props.id
        };
    }

    componentDidMount() {
        axios.post('/api/get/news/'+this.props.match.params.id).then(response => {
            this.setState({
                newss : response.data.data,
                total: response.data.total,
                per_page: response.data.per_page,
                activePage:response.data.current_page
            });
        }).catch(error => {
            console.log(error)
        })
    }

    handlePageChange(pageNumber) {
        console.log('change')
        this.setState({activePage: pageNumber});
        axios.get('/api/get/news/'+this.props.id+'?page='+pageNumber).then(response => {
            this.setState({
                newss : response.data.data ,
                total:response.data.total,
                per_page: response.data.per_page,
                activePage:response.data.current_page,
                id:this.props.id
            });
        }).catch(errors => {
            console.log(errors);
        });
        window.location.hash = pageNumber
    }

    deletenews(e){
        const news_id = +(e.target.getAttribute('data-id'));
        axios.delete('/api/news/'+news_id).then(response => {
            const newss = this.state.newss.filter(news => news_id !== news.id);
            this.setState({newss});
        }).catch(error => {
            console.log(error)
        })
    }

    render() {
        const id = this.props.id;
        return (
            <React.Fragment>
                <PageHeader heading="My news"/>
                <div className="row">
                    <table className="table bg-light" width="100%" >
                        <thead>
                        <tr>
                            <th className={'text-center'}>Title</th>
                            <th className={'text-center'}>Body</th>
                            <th className={'text-center'}>Image</th>
                            <th className={'text-center'} colSpan="3" >Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        {this.state.newss.map((news) =>
                            <tr key={news.id}>
                                <td className={'text-center'}>{news.title}</td>
                                <td className={'text-center'} dangerouslySetInnerHTML={{ __html: news.description }}></td>
                                <td className={'text-center'}><Link to={'/news/'+news.id}><img src={"/uploads/"+news.image} height='50' alt=""/></Link></td>
                                <td className={'text-center'}><Link to={'/edit/news/'+news.id}><i className={'fas fa-edit orange'}></i></Link></td>
                                <td className={'text-center'}><Link to={'/news/'+news.id}><i className={'fas fa-eye green'}></i></Link></td>
                                <td className={'text-center'}><i onClick={this.deletenews.bind(this)} data-id={news.id} className="btn bg-transparent red fas fa-trash-alt"></i></td>
                            </tr>
                        )}
                        </tbody>
                    </table>
                    <div className={'col-sm-12 mt-5'}>
                        <div className="row justify-content-center">
                            {this.state.total > this.state.per_page &&
                                <Pagination
                                    activePage={this.state.activePage}
                                    itemsCountPerPage={this.state.per_page}
                                    totalItemsCount={this.state.total}
                                    pageRangeDisplayed={5}
                                    onChange={this.handlePageChange.bind(this)}
                                    itemClass={'page-item'}
                                    linkClass={'page-link'}
                                    prevPageText={'Previous'}
                                    firstPageText="First"
                                    lastPageText="Last"
                                    nextPageText="Next"
                                />
                            }
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default Page;