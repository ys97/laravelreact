import React from 'react'
import 'bootstrap/dist/js/bootstrap.bundle';
import PageHeader from '../../common/pageHeader';
import axios from 'axios';
import {Link} from "react-router-dom";
import '@fortawesome/fontawesome-free/css/all.css';
import Pagination from "react-js-pagination";

class Page extends React.Component {
    constructor(props) {
        super(props);
        const hash = window.location.hash.substring(1)
        this.state = {
           news:[]
        };
    }

    componentDidMount() {
        axios.get('/api/news/'+this.props.match.params.id).then(response => {
            this.setState({
                news:response.data
            });
        }).catch(error => {
            console.log(error)
        })
    }

    editnews(e) {
        e.preventDefault();
        event.preventDefault();
        document.getElementById('submit_btn').setAttribute('disabled',true)
        document.getElementById('submit_btn').innerHTML = ('Editing...')
        const formData = new FormData();
        formData.append("title", document.querySelector('#news_title').value);
        formData.append("description", document.querySelector('#news_body').value);
        formData.append('_method', 'PUT'); // ADD THIS LINE
        axios({
            method: 'POST', //CHANGE TO POST
            url: '/api/news/'+this.props.match.params.id,
            data: formData,
        }).then(response => {
            this.setState({news:response.data})
            setInterval(function () {
                document.getElementById('submit_btn').removeAttribute('disabled')
                document.getElementById('submit_btn').innerHTML = ('Submit')
            },1000)
        }).catch(response => {
            this.setState({msg:response.message})
            this.setState({msgClass:'alert-danger'})
            setInterval(function () {
                document.getElementById('submit_btn').removeAttribute('disabled')
                document.getElementById('submit_btn').innerHTML = ('Try again...')
            },1000)
        })
    }

    deletenews(e){
        const news_id = +(e.target.getAttribute('data-id'));
        axios.delete('/api/newss/'+news_id).then(response => {
            const newss = this.state.newss.filter(news => news_id !== news.id);
            this.setState({newss});
        }).catch(error => {
            console.log(error)
        })
    }

    render() {
        const news = this.state.news;
        return (
            <React.Fragment>
                <PageHeader heading="Edit news"/>
                <div className="row jcc pt-5">
                   <div className="col-sm-4">
                       <form onSubmit={this.editnews.bind(this)} className={'text-center'}>
                           <input type="text" name={'news_title'} className='form-control-plaintext text-light' id={'news_title'} key={`${Math.floor((Math.random() * 1000))}-min`} defaultValue={this.state.news.title} autoComplete={'off'}/>
                           <div className={'form-group pt-3'}>
                               <textarea className='textarea form-control-plaintext text-light' name={'news_body'} key={`${Math.floor((Math.random() * 1000))}-min`} defaultValue={news.description} id={'news_body'}></textarea>
                           </div>
                           <button type={'submit'} className={'btn pr-5 btn-block pl-5 btn-outline-danger'} id={'submit_btn'}>Submit</button>
                       </form>
                   </div>
                </div>
            </React.Fragment>
        );
    }
}

export default Page;