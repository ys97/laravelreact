import React from 'react'
import 'bootstrap/dist/js/bootstrap.bundle';
import PageHeader from '../../common/pageHeader';
import axios from 'axios';
import {Link} from "react-router-dom";
import '@fortawesome/fontawesome-free/css/all.css';
import Pagination from "react-js-pagination";

class Page extends React.Component {
    constructor(props) {
        super(props);
        const hash = window.location.hash.substring(1)
        this.state = {
           post:[]
        };
    }

    componentDidMount() {
        axios.get('/api/posts/'+this.props.match.params.id).then(response => {
            this.setState({
                post:response.data
            });
        }).catch(error => {
            console.log(error)
        })
    }

    editPost(e) {
        e.preventDefault();
        document.getElementById('submit_btn').setAttribute('disabled',true)
        document.getElementById('submit_btn').innerHTML = ('Editing...')
        const formData = new FormData();
        formData.append("title", document.querySelector('#post_title').value);
        formData.append("body", document.querySelector('#post_body').value);
        formData.append("excerpt", document.querySelector('#excerpt').value);
        formData.append('_method', 'PUT'); // ADD THIS LINE
        axios({
            method: 'post', //CHANGE TO POST
            url: '/api/posts/'+this.props.match.params.id,
            data: formData,
        }).then(response => {
            this.setState({post:response.data})
            setInterval(function () {
                document.getElementById('submit_btn').removeAttribute('disabled')
                document.getElementById('submit_btn').innerHTML = ('Submit')
            },1000)
        }).catch(response => {
            this.setState({msg:response.message})
            this.setState({msgClass:'alert-danger'})
            setInterval(function () {
                document.getElementById('submit_btn').removeAttribute('disabled')
                document.getElementById('submit_btn').innerHTML = ('Try again...')
            },1000)
        })
    }

    deletePost(e){
        const post_id = +(e.target.getAttribute('data-id'));
        axios.delete('/api/posts/'+post_id).then(response => {
            const posts = this.state.posts.filter(post => post_id !== post.id);
            this.setState({posts});
        }).catch(error => {
            console.log(error)
        })
    }

    render() {
        const post = this.state.post;
        return (
            <React.Fragment>
                <PageHeader heading="Edit Post"/>
                <div className="row jcc pt-5">
                   <div className="col-sm-4 pb-5">
                       <form onSubmit={this.editPost.bind(this)} className={'text-center'}>
                           <div className={'form-group pt-3'}>
                                <input type="text" name={'post_title'} className={'form-control-plaintext text-light'} id={'post_title'} key={`${Math.floor((Math.random() * 1000))}-min`} defaultValue={this.state.post.title} autoComplete={'off'}/>
                           </div>
                           <div className={'form-group pt-3'}>
                               <textarea className={'textarea form-control-plaintext text-light'} name={'excerpt'} key={`${Math.floor((Math.random() * 1000))}-min`} defaultValue={post.excerpt} id={'excerpt'}></textarea>
                           </div>
                           <div className={'form-group pt-3'}>
                               <textarea className={'textarea form-control-plaintext text-light'} name={'post_body'} key={`${Math.floor((Math.random() * 1000))}-min`} defaultValue={post.body} id={'post_body'}></textarea>
                           </div>
                           <button type={'submit'} className={'btn pr-5 pl-5 mt-3 btn-block btn-outline-danger'} id={'submit_btn'}>Submit</button>
                       </form>
                   </div>
                </div>
            </React.Fragment>
        );
    }
}

export default Page;