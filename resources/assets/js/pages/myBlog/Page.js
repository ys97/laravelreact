import React from 'react'
import 'bootstrap/dist/js/bootstrap.bundle';
import PageHeader from '../../common/pageHeader';
import axios from 'axios';
import {Link} from "react-router-dom";
import '@fortawesome/fontawesome-free/css/all.css';
import Pagination from "react-js-pagination";

class Page extends React.Component {
    constructor(props) {
        super(props);
        const hash = window.location.hash.substring(1)
        this.state = {
            blogs:[],
            activePage:1,
            page:hash,
            id:this.props.id
        };
    }

    componentDidMount() {
        axios.post('/api/get/blog/'+this.props.match.params.id).then(response => {
            this.setState({
                blogs : response.data.data,
                total: response.data.total,
                per_page: response.data.per_page,
                activePage:response.data.current_page
            });
        }).catch(error => {
            console.log(error)
        })
    }

    handlePageChange(pageNumber) {
        console.log('change')
        this.setState({activePage: pageNumber});
        axios.get('/api/get/blog/'+this.props.id+'?page='+pageNumber).then(response => {
            this.setState({
                blogs : response.data.data ,
                total:response.data.total,
                per_page: response.data.per_page,
                activePage:response.data.current_page,
                id:this.props.id
            });
        }).catch(errors => {
            console.log(errors);
        });
        window.location.hash = pageNumber
    }

    deleteblog(e){
        const blog_id = +(e.target.getAttribute('data-id'));
        axios.delete('/api/blog/'+blog_id).then(response => {
            const blogs = this.state.blogs.filter(blog => blog_id !== blog.id);
            this.setState({blogs});
        }).catch(error => {
            console.log(error)
        })
    }

    render() {
        const id = this.props.id;
        return (
            <React.Fragment>
                <PageHeader heading="My blog"/>
                <div className="row">
                    <table className="table bg-light" width="100%" >
                        <thead>
                        <tr>
                            <th className={'text-center'}>Title</th>
                            <th className={'text-center'}>Body</th>
                            <th className={'text-center'}>Image</th>
                            <th className={'text-center'} colSpan="3" >Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        {this.state.blogs.map((blog) =>
                            <tr key={blog.id}>
                                <td className={'text-center'}>{blog.title}</td>
                                <td className={'text-center'} dangerouslySetInnerHTML={{ __html: blog.body }}></td>
                                <td className={'text-center'}><Link to={'/blog/'+blog.id}><img src={"/uploads/"+blog.image} height='50' alt=""/></Link></td>
                                <td className={'text-center'}><Link to={'/edit/blog/'+blog.id}><i className={'fas fa-edit orange'}></i></Link></td>
                                <td className={'text-center'}><Link to={'/blog/'+blog.id}><i className={'fas fa-eye green'}></i></Link></td>
                                <td className={'text-center'}><i onClick={this.deleteblog.bind(this)} data-id={blog.id} className="btn bg-transparent red fas fa-trash-alt"></i></td>
                            </tr>
                        )}
                        </tbody>
                    </table>
                    <div className={'col-sm-12 mt-5'}>
                        <div className="row justify-content-center">
                            { this.state.total > this.state.per_page &&
                                <Pagination
                                    activePage={this.state.activePage}
                                    itemsCountPerPage={this.state.per_page}
                                    totalItemsCount={this.state.total}
                                    pageRangeDisplayed={5}
                                    onChange={this.handlePageChange.bind(this)}
                                    itemClass={'page-item'}
                                    linkClass={'page-link'}
                                    prevPageText={'Previous'}
                                    firstPageText="First"
                                    lastPageText="Last"
                                    nextPageText="Next"
                                />
                            }
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default Page;