import React from 'react'
import 'bootstrap/dist/js/bootstrap.bundle';
import PageHeader from '../../common/pageHeader';
import axios from 'axios';
import {Link} from "react-router-dom";
import '@fortawesome/fontawesome-free/css/all.css';
import Pagination from "react-js-pagination";

class Page extends React.Component {
    constructor(props) {
        super(props);
        const hash = window.location.hash.substring(1)
        this.state = {
           blog:[]
        };
    }

    componentDidMount() {
        axios.get('/api/blog/'+this.props.match.params.id).then(response => {
            this.setState({
                blog:response.data
            });
        }).catch(error => {
            console.log(error)
        })
    }

    editblog(e) {
        e.preventDefault();
        document.getElementById('submit_btn').setAttribute('disabled',true)
        document.getElementById('submit_btn').innerHTML = ('Editing...')
        event.preventDefault()
        const formData = new FormData();
        formData.append("title", document.querySelector('#blog_title').value);
        formData.append("body", document.querySelector('#blog_body').value);
        formData.append('_method', 'PUT'); // ADD THIS LINE
        axios({
            method: 'POST', //CHANGE TO POST
            url: '/api/blog/'+this.props.match.params.id,
            data: formData,
        }).then(response => {
            this.setState({blog:response.data})
            setInterval(function () {
                document.getElementById('submit_btn').removeAttribute('disabled')
                document.getElementById('submit_btn').innerHTML = ('Submit')
            },1000)
        }).catch(response => {
            this.setState({msg:response.message})
            this.setState({msgClass:'alert-danger'})
            setInterval(function () {
                document.getElementById('submit_btn').removeAttribute('disabled')
                document.getElementById('submit_btn').innerHTML = ('Try again...')
            },1000)
        })
    }

    deleteblog(e){
        const blog_id = +(e.target.getAttribute('data-id'));
        axios.delete('/api/blogs/'+blog_id).then(response => {
            const blogs = this.state.blogs.filter(blog => blog_id !== blog.id);
            this.setState({blogs});
        }).catch(error => {
            console.log(error)
        })
    }

    render() {
        const blog = this.state.blog;
        return (
            <React.Fragment>
                <PageHeader heading="Edit blog"/>
                <div className="row jcc pt-5">
                   <div className="col-sm-4">
                       <form onSubmit={this.editblog.bind(this)} className={'text-center'}>
                           <input type="text" name={'blog_title'} className={'form-control-plaintext text-light'} id={'blog_title'} key={`${Math.floor((Math.random() * 1000))}-min`} defaultValue={this.state.blog.title} autoComplete={'off'}/>
                           <div className={'form-group pt-3'}>
                               <textarea className={'textarea form-control-plaintext text-light'} name={'blog_body'} key={`${Math.floor((Math.random() * 1000))}-min`} defaultValue={blog.body} id={'blog_body'}></textarea>
                           </div>
                           <button type={'submit'} className={'btn btn-block pr-5 pl-5 btn-outline-danger'} id="submit_btn">Submit</button>
                       </form>
                   </div>
                </div>
            </React.Fragment>
        );
    }
}

export default Page;