import React, {Component} from 'react';
import {Link} from "react-router-dom";
import axios from 'axios';

class Navbar extends Component {

    constructor(){
        super();
        this.state = {
            menu:[],
        }
    }

    componentDidMount() {
        axios.get('/api/menu').then(response => {
            this.setState({menu:response.data})
        })
    }

    render() {
        const menu = this.state.menu;
        // const locale = this.state.locale == 'en'?'Arm':'En'
        return (
            <React.Fragment>
                <nav className="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                            aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse justify-content-end" id="navbarNav">
                        <ul className="navbar-nav">
                            {this.props.loggedIn &&
                            <li className={'nav-item'}>
                                <Link className={'nav-link '} to='/profile'>Profile <span className="sr-only"></span></Link>
                            </li>}
                            {menu.map(item =>
                                <li className={window.location.pathname == item.url?'nav-item active':'nav-item'} key={item.id}>
                                    <Link className={'nav-link '} to={item.url}>{item.title} <span className="sr-only"></span></Link>
                                </li>
                            )}
                            {this.props.loggedIn &&
                            <li className={'nav-item'}>
                                <Link className={'nav-link'} to='/logout'>Logout<span className="sr-only"></span></Link>
                            </li>}
                            {!this.props.loggedIn &&
                            <li className={'nav-item'}>
                                <Link className={'nav-link'} to='/login'>Login<span className="sr-only"></span></Link>
                            </li>}
                            {!this.props.loggedIn &&
                            <li className={'nav-item'}>
                                <Link className={'nav-link'} to='/register'>Sign Up<span className="sr-only"></span></Link>
                            </li>}
                            <li className={this.props.lang == 'en'?'active':''}><button className={'nav-link btn bg-transparent'} onClick={()=>this.props.handleClick('en')}>Eng</button></li>
                            <li className={this.props.lang == 'hy'?'active':''}><button className={'nav-link btn bg-transparent'} onClick={()=>this.props.handleClick('hy')}>Arm</button></li>
                        </ul>
                    </div>
                </nav>
            </React.Fragment>
        );
    }
}

export default Navbar;
