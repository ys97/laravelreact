import React, {Component} from 'react';
import axios from 'axios';
import {Link} from "react-router-dom";
import Pagination from 'react-js-pagination';
import PageHeader from "../common/pageHeader";

class News extends Component {
    constructor(){
        super();
        const hash = window.location.hash.substring(1)
        this.state = {
            news:[],
            page:hash,
            activePage:1
        }
    }
    componentDidMount() {
        axios.get('/api/news?page='+this.state.page).then(response => {
            this.setState({
                news:response.data.data,
                activePage:response.data.current_page,
                total:response.data.total,
                per_page:response.data.per_page,
            })
        }).catch(error => {
            console.log(error);
        })
    }
    changePage(page){
        axios.get('/api/news?page='+page).then(response => {
            this.setState({
                news:response.data.data,
                activePage:response.data.current_page,
                total:response.data.total,
                per_page:response.data.per_page,
            })
        }).catch(error => {
            console.log(error);
        })
        window.location.hash = page;
    }
    render() {
        const news = this.state.news;
        return (
            <React.Fragment>
                <section className={'h_full'}>
                    <PageHeader heading="News"/>
                    <div className="container pt-5">
                        {news.map(n =>
                            <div key={n.id} className="row jcc posts_row p-5 mb-5">
                                <div className="col-sm-5">
                                    <Link to={'/news/'+n.id} className={'img_link'}><img src={'/uploads/'+n.image} className={'img-fluid'} alt=""/></Link>
                                </div>
                                <div className="col-sm-7">
                                    <h3 className={'text-light'}>{n.title}</h3>
                                    <p className={'text-light'}>{n.description}</p>
                                    <Link to={"/news/"+n.id} className={'btn btn-outline-danger'}>Read More</Link>
                                </div>
                            </div>
                        )}
                        <div className="row jcc mt-4">
                            <Pagination
                                activePage={this.state.activePage}
                                totalItemsCount={this.state.total}
                                itemsCountPerPage={this.state.per_page}
                                itemClass={'page-item'}
                                linkClass={'page-link'}
                                prevPageText={'Previous'}
                                firstPageText="First"
                                lastPageText="Last"
                                nextPageText="Next"
                                onChange={this.changePage.bind(this)}
                        />
                        </div>
                    </div>
                </section>
            </React.Fragment>
        );
    }
}

export default News;
