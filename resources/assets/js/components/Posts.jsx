import React, {Component} from 'react';
import {Link} from "react-router-dom";
import axios from 'axios';
import Pagination from 'react-js-pagination';
import PageHeader from "../common/pageHeader";


class Posts extends Component {

    constructor(){
        super();
        const hash = window.location.hash.substring(1)
        this.state = {
            posts:[],
            page:hash,
            activePage:1
        }
    }

    componentDidMount() {
        axios.get('/api/posts?page='+this.state.page).then(response => {
            this.setState({
                posts:response.data.data,
                activePage:response.data.current_page,
                total:response.data.total,
                per_page:response.data.per_page
            })
        }).catch(error => {
            console.log(error)
        })
    }

    changePage(number){
        axios.get('/api/posts?page='+number).then(response => {
            this.setState({
                posts:response.data.data,
                activePage:number,
                total:response.data.total,
                per_page:response.data.per_page
            })
        }).catch(error => {
            console.log(error)
        })
        window.location.hash = number
    }

    render() {
        const posts = this.state.posts;
        return (
            <React.Fragment>
                <section className={'h_full'}>
                    <PageHeader heading="Posts"/>
                    <div className="pt-5 container">
                        {posts.map(p =>
                            <div key={p.id} className={'row jcc posts_row mb-5 p-5'}>
                                <div className="col-sm-5">
                                    <img src={'/uploads/'+p.image} className={'img-fluid'} alt=""/>
                                </div>
                                <div className="col-sm-7">
                                    <h3 className={'text-light'}>{p.title}</h3>
                                    <p className={'text-light'}>{p.excerpt}</p>
                                    <Link to={"/post/"+p.id} className={'btn btn-outline-danger'}>Read More</Link>
                                </div>
                            </div>
                        )}
                        <div className="row jcc mt-5">
                            <Pagination
                                activePage={this.state.activePage}
                                totalItemsCount={this.state.total}
                                itemsCountPerPage={this.state.per_page}
                                itemClass={'page-item'}
                                linkClass={'page-link'}
                                prevPageText={'Previous'}
                                firstPageText="First"
                                lastPageText="Last"
                                nextPageText="Next"
                                onChange={this.changePage.bind(this)}
                            />
                        </div>
                    </div>
                </section>
            </React.Fragment>
        );
    }
}

export default Posts;
