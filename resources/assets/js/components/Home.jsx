import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import axios from 'axios';
import Sidebar from './Sidebar'
import PageHeader from "../common/pageHeader";

class Home extends Component {

    constructor(props){
        super(props)
        this.state = {
            blog:[],
            posts:[]
        }
    }

    componentDidMount() {
        axios.get('/api/blog').then(response=>{
            this.setState({
                blog:response.data
            })
        });
        axios.get('/api/posts').then(response=>{
            this.setState({
                posts:response.data
            })
        })
    }

    render() {
        const blog = this.state.blog;
        const posts = this.state.posts;
        return (
            <React.Fragment>
                <PageHeader heading="Home"/>
                <div className={'container-fluid pt-5 '}>
                    <div className="row jcc mb-5">
                        <h3 className={'main_title text-light'}>Our Blog</h3>
                    </div>
                    <div className="row jcc">
                        {blog.map(b =>
                            <div key={b.id} className={'col-sm-4 p-3'}><img src={'/uploads/'+b.image} height="450px" alt={b.title} className="img-fluid"/></div>
                        )}
                    </div>
                </div>
                <div className="container-fluid">
                    <div className="row jcc">
                        <div className="col-md-12 col-lg-8">
                            <div className="row jcc mt-5">
                                <h3 className={'main_title text-light'}>Posts</h3>
                            </div>
                            <div className="row mt-5">
                            {posts.map(p =>
                                <div className={'col-sm-6'} key={p.id}>
                                    <Link to={'/post/'+p.id} className={'img_link'}><img src={'/uploads/'+p.image} className={'img-fluid'} alt=""/></Link>
                                    <Link to={'/post/'+p.id} className={'main_link'}><h3 className={'text-light'}>{p.title}</h3></Link>
                                    <p className={'text-light'}>{p.excerpt}</p>
                                </div>
                            )}
                            </div>
                        </div>
                        <Sidebar lang={this.props.lang}/>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default Home;
