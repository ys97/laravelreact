import React, {Component} from 'react';
import {Link} from 'react-router-dom';

class NotFound extends Component {
    render() {
        return (
            <React.Fragment>
                <section className='pt-4 bg-dark h_full'>
                    <div className="container-fluid h-100">
                        <div className="row jcc h-100 align-items-center">
                            <div className="col-sm-12 text-center">
                                <div className="col-sm-12 text-center">
                                    <h1 className='text-light'>404</h1>
                                </div>
                                <div className="col-sm-12 text-center mt-5 mb-5">
                                    <h3 className={'text-light'}>Sorry ,but page not found</h3>
                                </div>
                                <div className="col-sm-12 text-center">
                                    <Link to={'/'} className={'btn btn-outline-danger btn-'}>Back to Home</Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </React.Fragment>
        );
    }
}

export default NotFound;
