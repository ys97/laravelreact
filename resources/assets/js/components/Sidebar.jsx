import React, {Component} from 'react';
import axios from 'axios';
import {Link} from 'react-router-dom'

class Sidebar extends Component {

    constructor() {
        super();
        this.state = {
            news:[]
        }
    }

    componentWillMount() {
        axios.get('/api/news').then(response => {
            this.setState({
                news:response.data
            })
        })
    }

    render() {
        const news = this.state.news;
        return (
            <React.Fragment>
                <div className="col-md-12 col-lg-4">
                    <div className="row jcc mt-5 mb-5">
                        <h3 className={'main_title text-light'}>News</h3>
                    </div>
                    {news.map(n =>
                        <div key={n.id} className="row jcc mt-1 posts_row p-3">
                            <div className={'col-sm-5'}>
                                <Link to={'/news/'+n.id} className={'img_link'}>
                                    <img src={'uploads/'+n.image} className={'img-fluid bordered'}/>
                                </Link>
                            </div>
                            <div className={'col-sm-7'}>
                                <Link to={'/news/'+n.id} className={'main_link'}><h6 className={'text-light'}>{n.title}</h6></Link>
                                <p className={'text-light'}>{n.description}</p>
                            </div>
                        </div>
                    )}
                </div>
            </React.Fragment>
        );
    }
}

export default Sidebar;
