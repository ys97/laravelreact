import React, {Component} from 'react';
import axios from 'axios';

class ViewNews extends Component {
    constructor(props) {
        super(props);
        this.state = {
            news:[],
        }

    }
    componentDidMount() {
        const id = this.props.match.params.id;
        axios.get('/api/news/'+id).then(response => {
            this.setState({
                news:response.data
            })
        })
    }
    render() {
        const news = this.state.news;
        return (
            <React.Fragment>
                <section className={'h_full'}>
                    <div className="container pt-5">
                        <div className="row jcc">
                            <h3 className={'main_title text-light'}>{news.title}</h3>
                        </div>
                        <div className="row jcc">
                            <div className="col-sm-12 pt-5 pb-5">
                                <div className="row jcc">
                                    <a href="javascript:void(0)" className={'img_link'}><img src={'/uploads/'+news.image} className={'img-fluid mt-3'} alt=""/></a>
                                </div>
                            </div>
                            <div className="col-sm-12 mt-3 text-light" dangerouslySetInnerHTML={{ __html: news.description }}/>
                        </div>
                    </div>
                </section>
            </React.Fragment>
        );
    }
}

export default ViewNews;
