import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import axios from 'axios';
import Pagination from 'react-js-pagination';
import PageHeader from "../common/pageHeader";

class Blog extends Component {

    constructor(){
        super();
        const hash = window.location.hash.substring(1)
        this.state = {
            blogs:[],
            activePage:1,
            page:hash
        };
    }

    componentDidMount() {
        axios.get('/api/blog?page='+this.state.page).then(response => {
            this.setState({
                blogs : response.data.data,
                total: response.data.total,
                per_page: response.data.per_page,
                activePage:response.data.current_page
            });
        }).catch(errors => {
            console.log(errors);
        });
    }

    handlePageChange(pageNumber) {
        this.setState({activePage: pageNumber});
        axios.get('/api/blog?page='+pageNumber).then(response => {
            this.setState({
                blogs : response.data.data ,
                total:response.data.total,
                per_page: response.data.per_page,
                activePage:response.data.current_page
            });
        }).catch(errors => {
            console.log(errors);
        });
        window.location.hash = pageNumber
    }

    render() {
        return (
            <React.Fragment>
                <section className={'h_full'}>
                    <PageHeader heading="Blog"/>
                    <div className={'container-fluid pt-5'}>
                        <div className='row justify-content-center'>
                            {this.state.blogs.map((b)=>
                                <div key={b.id} className={'col-sm-4'}>
                                    <div className={'row justify-content-center'}>
                                        <div className={'col-sm-12 text-center'} style={{height:'60px'}}>
                                            <h1><Link to={'/blog/'+b.id} className={'main_link text-light'}>{b.title}</Link></h1>
                                        </div>
                                        <div className={'col-sm-12 text-center'}>
                                            <Link to={'/blog/'+b.id} params={{blogId:b.id}} className={'img_link'}><img src={'/uploads/'+b.image} className={'img-fluid'} alt=""/></Link>
                                        </div>
                                        <div className={'col-sm-12 text-center text-light'}>
                                            {b.body}
                                        </div>
                                    </div>
                                </div>
                            )}
                        </div>
                        <div className={'col-sm-12 mt-5'}>
                            <div className="row justify-content-center">
                                <Pagination
                                    activePage={this.state.activePage}
                                    itemsCountPerPage={this.state.per_page}
                                    totalItemsCount={this.state.total}
                                    pageRangeDisplayed={5}
                                    onChange={this.handlePageChange.bind(this)}
                                    itemClass={'page-item'}
                                    linkClass={'page-link'}
                                    prevPageText={'Previous'}
                                    firstPageText="First"
                                    lastPageText="Last"
                                    nextPageText="Next"
                                />
                            </div>
                        </div>
                    </div>
                </section>
            </React.Fragment>
        );
    }

}

export default Blog;
