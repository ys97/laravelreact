import React, {Component} from 'react';
import axios from 'axios';

class ViewBlog extends Component {

    constructor(){
        super();
        this.state={
            blog:[]
        }
    }

    componentDidMount(){
        axios.get('/api/blog/'+this.props.match.params.id).then(response=>{
            this.setState({blog:response.data})
        }).catch(error=>{
            console.log(error)
        })
    }

    render() {
        return (
            <React.Fragment>
                <section className={'h_full'}>
                    <div className={'container pt-5'}>
                        <div className={'row justify-content-center'}>
                            <div className={'col-sm-12 text-center'}>
                                <a href="javascript:void(0)" className={'img_link'}><img src={'/uploads/'+this.state.blog.image} className={'img-fluid'} alt=""/></a>
                            </div>
                            <div className={'col-sm-12 text-center pt-5'}>
                                <h3 className={'main_title text-light'}>{this.state.blog.title}</h3>
                                <p className={'text-light mt-5'}>{this.state.blog.body}</p>
                            </div>
                        </div>
                    </div>
                </section>
            </React.Fragment>
        );
    }
}

export default ViewBlog;
