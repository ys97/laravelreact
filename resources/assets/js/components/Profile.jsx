import React, {Component} from 'react';
import axios from 'axios';
import {Redirect} from "react-router-dom";

class Profile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user:[],
        }

    }
    componentWillMount() {
        const id = this.props.id;
        axios.get('/api/profile/'+id).then(response => {
            this.setState({
                user:response.data
            })
        })
    }
    render() {
        const id = this.props.id;
        if(!id){
            return <Redirect to={'/'}/>;
        }
        const user = this.state.user;
        return (
            <React.Fragment>
                <section className={'h_full'}>
                    <div className="container pt-5">
                        <div className="row jcc">
                            <h3 className={'main_title text-light'}>{user.name}</h3>
                        </div>
                        <div className="row jcc">
                            <div className="col-sm-12 mt-3 text-light" dangerouslySetInnerHTML={{ __html: user.email }}/>
                        </div>
                    </div>
                </section>
            </React.Fragment>
        );
    }
}

export default Profile;
