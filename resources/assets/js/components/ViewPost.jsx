import React, {Component} from 'react';
import axios from 'axios';


class ViewPost extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id:this.props.id,
            post:[],
        }
    }

    componentDidMount() {
        axios.get('/api/posts/'+this.props.match.params.id).then(response => {
            this.setState({post:response.data})
        }).catch(error => {
            console.log(error)
        })
    }

    render() {
        const post = this.state.post;
        return (
            <React.Fragment>
                <section className={'h_full'}>
                    <div className="container pt-5 h-100">
                        <div className="row jcc h-100">
                            <div className="col-sm-12 text-center">
                                <h3 className={'main_title text-light'}>{post.title}</h3>
                            </div>
                            <div className="col-sm-12 text-center pt-5 pb-5">
                                <img src={'/uploads/'+post.image} className={'img-fluid mt-3'} alt=""/>
                            </div>
                            <div className="col-sm-12 mt-3 text-light" dangerouslySetInnerHTML={{ __html: post.body }}/>
                        </div>
                    </div>
                </section>
            </React.Fragment>
        );
    }
}

export default ViewPost;
