/**
 * Created by Sumit-Yadav on 06-10-2017.
 */
import React from 'react'
import {NavLink, Link} from 'react-router-dom'
import PropTypes from 'prop-types'
import {
    Button,
    Container,
    Dropdown,
    Divider,
    Image,
    Menu,
    Responsive,
    Segment
} from 'semantic-ui-react';
import axios from 'axios'
import * as actions from '../../store/actions'


class Page extends React.Component {
    constructor(props) {
        super(props);
        this.handleLogout = this.handleLogout.bind(this);
        this.state = {
            menu:[]
        }
    }

    handleLogout() {
        event.preventDefault();
        this.props.dispatch(actions.authLogout());
    }

    componentDidMount() {
        axios.get('/api/menu').then(response => {
            this.setState({menu:response.data})
        })
    }
    render() {
        const menu = this.state.menu;
        this.avatar = (
            <span>
                 <Image avatar src={require('../../../images/avatar/boy.png')}
                        verticalAlign='top'/> {this.props.userName}
            </span>
        );
        return (

            <React.Fragment>
                <nav className="navbar navbar-expand-lg navbar-dark bg-dark ">
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                            aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse justify-content-end" id="navbarNav">
                        <ul className="navbar-nav">
                            {this.props.isAuthenticated
                                ?
                                <React.Fragment>
                                <li>
                                    <Link to={'/dashboard'} className={'nav-link'} >
                                        Dashboard
                                    </Link>
                                </li>
                                <li>
                                    <Link to={'/my_posts/'+this.props.id} className={'nav-link'}>
                                        My Posts
                                    </Link>
                                </li>
                                <li>
                                    <Link to={'/my_blog/'+this.props.id} className={'nav-link'}>
                                        My Blog
                                    </Link>
                                </li>
                                <li>
                                    <Link to={'/my_news/'+this.props.id} className={'nav-link'}>
                                        My News
                                    </Link>
                                </li>
                                </React.Fragment>
                                :''}
                            {menu.map(item =>
                                <li className={window.location.pathname == item.url?'nav-item active':'nav-item'} key={item.id}>
                                    <Link className={'nav-link '} to={item.url}>{item.title} <span className="sr-only"></span></Link>
                                </li>
                            )}
                            {this.props.isAuthenticated
                                ?
                                    <li><span onClick={this.handleLogout} className={'nav-link bg-transparent'} key='logout'>Logout</span></li>
                                    // <Button>
                                    //     Logout
                                    // </Button>
                                :
                                <React.Fragment>
                                    <li>
                                        <Link to={'/login'} text="login" className={'nav-link'}>Login</Link>
                                    </li>
                                    <li>
                                        <Link to={'/register'} text="register" className={'nav-link'}>Register</Link>
                                    </li>
                                </React.Fragment>
                            }

                        </ul>
                    </div>
                </nav>
            </React.Fragment>
        );
    }
}

Page.propTypes = {
    dispatch: PropTypes.func.isRequired,
};

export default Page;