import React from 'react'
import {connect} from 'react-redux'
import Navigation from './common/navigation'
import Footer from './common/mainFooter'

class Main extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <React.Fragment>
                <Navigation/>
                <section className="fadeIn animated">
                    {this.props.children}
                </section>
            </React.Fragment>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        isAuthenticated: state.Auth.isAuthenticated
    }
};

export default connect(mapStateToProps)(Main);